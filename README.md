# lsp-utils

Utilitaires de déploiement des serveurs LSP (Language Server Protocol).

Conçu pour le projet [Caseine](https://moodle.caseine.org/)


(C) Silecs 2023
Sauf mention contraire explicite, tous les fichiers du projet sont placés sous licence [Affero GPL 3](https://www.gnu.org/licenses/agpl-3.0.fr.html)